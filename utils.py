import ast

import pandas as pd
import torch
import PIL.Image as Image
import os
import torchvision.transforms as transforms
import numpy as np

def load_training_results(filename, size=5000):
    """
    Load training expected results from a file.
    """
    data = pd.read_csv(filename).head(size)
    data['tom'] = pd.to_numeric(data['tom'], downcast='float')
    data['jerry'] = pd.to_numeric(data['jerry'], downcast='float')
    tomnjerry = data.iloc[:, -2:]
    tuples = tomnjerry.apply(lambda x: [x[0], x[1]], axis=1)
    data.drop(data.columns[-2:], axis=1, inplace=True)
    data['result'] = tuples
    return data

def load_testing_results(filename, size=5000):
    """
    Load testing expected results from a file.
    """
    data = pd.read_csv(filename).tail(-size)
    data['tom'] = pd.to_numeric(data['tom'], downcast='float')
    data['jerry'] = pd.to_numeric(data['jerry'], downcast='float')
    tomnjerry = data.iloc[:, -2:]
    tuples = tomnjerry.apply(lambda x: [x[0], x[1]], axis=1)
    data.drop(data.columns[-2:], axis=1, inplace=True)
    data['result'] = tuples
    return data

def batch_data(data, batch_size):
    """
    Batch data into batches of size batch_size.
    """
    torch.utils.data.DataLoader(data, batch_size=batch_size)
    temp_data = data.sample(frac=1)
    batches = []
    for i in range(0, len(temp_data), batch_size):
        batches.append(temp_data[i:i+batch_size])
    return batches

def load_batch_images(batch, folder="archive/data/"):
    """
    Load images from a batch of data.
    """

    image_folder = folder
    image_paths = [os.path.join(image_folder, filename) for filename in batch.iloc[:, 0]]

    images = [np.array(Image.open(image_path), dtype=np.float32).flatten() for image_path in image_paths]

    return np.array(images)



def load_batch_images_3D(batch, folder="archive/data/"):
    """
    Load images from a batch of data.
    """
    image_folder = folder
    image_paths = [os.path.join(image_folder, filename) for filename in batch.iloc[:, 0]]

    images = [np.array(Image.open(image_path), dtype=np.float32) for image_path in image_paths]
    return np.array(images)








def load_batch_images_translated(batch, folder="archive/data/", type = "vertical"):
    """
    Load images from a batch of data. and translates them
    """
    image_folder = folder
    image_paths = [os.path.join(image_folder, filename) for filename in batch.iloc[:, 0]]

    images = [np.array(Image.open(image_path), dtype=np.float32) for image_path in image_paths]

    if type == "vertical":
        for i in range(len(images)):
            images[i] = np.roll(images[i], 1, axis=0)
    elif type == "horizontal":
        for i in range(len(images)):
            images[i] = np.roll(images[i], 1, axis=1)
    else:
        raise Exception("Invalid type")

    return np.array(images)

def load_batch_images_zoomed(batch, folder="archive/data/"):
    """
    Load images from a batch of data. and zooms them in 50%
    """
    image_folder = folder
    image_paths = [os.path.join(image_folder, filename) for filename in batch.iloc[:, 0]]

    images = [np.array(Image.open(image_path), dtype=np.float32) for image_path in image_paths]

    for i in range(len(images)):
        zoom_factor = 0.5
        h, w = images[i].shape[:2]
        new_h, new_w = int(zoom_factor * h), int(zoom_factor * w)
        top = (h - new_h)//2
        left = (w - new_w)//2
        images[i] = images[i][top:top + new_h, left:left + new_w]
        images[i] = np.array(Image.fromarray(images[i].astype(np.uint8)).resize((w, h)), dtype=np.float32)
    return np.array(images)


#data = pd.read_csv('archivev2/ground_truth.csv')
#data['tom'] = pd.to_numeric(data['tom'], downcast='float')
#data['jerry'] = pd.to_numeric(data['jerry'], downcast='float')
#tomnjerry = data.iloc[:, -2:]
#tuples = tomnjerry.apply(lambda x: [x[0], x[1]], axis=1)
#data.drop(data.columns[-2:], axis=1, inplace=True)
#data['result'] = tuples
#data.to_csv("archivev2/new_ground_truth.csv", index=False)
#for i in range(len(data)):
#    #resize images to 240p resolution
#    image_name = data['filename'][i]
#    image = Image.open("archivev2/tom_and_jerry/" + image_name)
#    image = image.resize((352, 240), Image.ANTIALIAS)
#    image.save("archivev2/tom_and_jerry/" + image_name)






base_set = pd.read_csv('archivev2/new_ground_truth.csv')
base_set.to_csv("archivev3/enhanced_ground_truth.csv", index=False)
base_size = len(base_set)
base_set = base_set.head(5000)
size = base_size

for i in range(5000):
    #write data to csv file
    image_name = "frame" + str(base_size+i+1) + ".jpg"
    #new_image = Image.open("archivev2/tom_and_jerry/" + base_set['filename'][i])
    #new_image = new_image.rotate(45)
    #new_image.save("archivev3/enhanced_data/" + image_name)
    base_set.iloc[i,0] = image_name


base_set.to_csv("archivev3/enhanced_ground_truth.csv", index=False, mode='a', header=False)
size += 5000

for i in range(5000):
    #write data to csv file
    base_name = "frame" + str(i+1) + ".jpg"
    image_name = "frame" + str(size+i+1) + ".jpg"
    #new_image = Image.open("archivev2/tom_and_jerry/" + base_name)
    #new_image = new_image.rotate(360-45)
    #new_image.save("archivev3/enhanced_data/" + image_name)
    base_set.iloc[i,0] = image_name

base_set.to_csv("archivev3/enhanced_ground_truth.csv", index=False, mode='a', header=False)
size += 5000

for i in range(5000):
    #write data to csv file
    base_name = "frame" + str(i+1) + ".jpg"
    image_name = "frame" + str(size+i+1) + ".jpg"
    #new_image = Image.open("archivev2/tom_and_jerry/" + base_name)
    #new_image = new_image.transpose(Image.FLIP_LEFT_RIGHT)
    #new_image.save("archivev3/enhanced_data/" + image_name)
    base_set.iloc[i,0] = image_name

base_set.to_csv("archivev3/enhanced_ground_truth.csv", index=False, mode='a', header=False)
size += 5000

for i in range(5000):
    #write data to csv file
    base_name = "frame" + str(i+1) + ".jpg"
    image_name = "frame" + str(size+i+1) + ".jpg"
    #new_image = Image.open("archivev2/tom_and_jerry/" + base_name)
    #new_image = new_image.transpose(Image.FLIP_TOP_BOTTOM)
    #new_image.save("archivev3/enhanced_data/" + image_name)
    base_set.iloc[i,0] = image_name

base_set.to_csv("archivev3/enhanced_ground_truth.csv", index=False, mode='a', header=False)
size += 5000



#base_set = pd.read_csv('archive/enhanced_ground_truth.csv')
#base_set['result'] = base_set['result'].apply(ast.literal_eval)
#translated_set = load_batch_images_translated(base_set, folder="archive/enhanced_data/", type="horizontal")
#translated_set_data = base_set.copy()
#
#for i in range(len(base_set)):
#    #write data to csv file
#    image_name = "frame" + str(len(base_set)+i+1) + ".jpg"
#    translated_set_data.iloc[i,0] = image_name
#    Image.fromarray(translated_set[i].astype(np.uint8)).save("archive/enhanced_data/" + image_name)
#
#translated_set_data.to_csv("archive/enhanced_ground_truth.csv", index=False, mode='a', header=False)
#
#size = size * 2
#base_set = pd.read_csv('archive/enhanced_ground_truth.csv')
#base_set['result'] = base_set['result'].apply(ast.literal_eval)
#translated_set = load_batch_images_zoomed(base_set.head(int(size/2)), folder="archive/enhanced_data/")
#translated_set_data = base_set.head(int(size/2))
#
#for i in range(len(base_set)//2):
#    #write data to csv file
#    image_name = "frame" + str(len(base_set)+i+1) + ".jpg"
#    translated_set_data.iloc[i,0] = image_name
#    Image.fromarray(translated_set[i].astype(np.uint8)).save("archive/enhanced_data/" + image_name)
#
#translated_set_data.to_csv("archive/enhanced_ground_truth.csv", index=False, mode='a', header=False)
#
#translated_set = load_batch_images_zoomed(base_set.tail(int(size/2)), folder="archive/enhanced_data/")
#translated_set_data = base_set.tail(int(size/2))
#
#for i in range(len(base_set)//2):
#    #write data to csv file
#    image_name = "frame" + str(int(len(base_set)+i+1+size//2)) + ".jpg"
#    translated_set_data.iloc[i,0] = image_name
#    Image.fromarray(translated_set[i].astype(np.uint8)).save("archive/enhanced_data/" + image_name)
#
#translated_set_data.to_csv("archive/enhanced_ground_truth.csv", index=False, mode='a', header=False)
#




