import torch
from torchvision.models import ResNet18_Weights


class FTNNResNet18():
    #a neural network using transfer learning from pretrained resnet18 (1000 classes)
    def __init__(self, resolution, lr=0.005):
        self.resolution = resolution
        self.loss = torch.nn.CrossEntropyLoss()
        self.model = torch.hub.load('pytorch/vision:v0.16.0', 'resnet18', weights=ResNet18_Weights.DEFAULT)
        self.fc = torch.nn.Linear(self.model.fc.in_features, 2)
        self.model.fc = torch.nn.Sequential(self.fc, torch.nn.Sigmoid())
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=lr)

    def forward(self, x):
        return self.model(x)

    def predict(self, x, batch_size=1):
        self.model.eval()
        with torch.no_grad():
            return self.forward(x)

    def train_on_batch(self, image_batch, result_batch):
        self.model.train()
        self.optimizer.zero_grad()
        y_pred = self.forward(image_batch)
        loss = self.loss(y_pred, result_batch)
        loss.backward()
        self.optimizer.step()
        return loss.item()

    def test(self, image_batch, test_batch, precision = 0.5):
        results = 0
        with torch.no_grad():
            y_predicted = self.predict(image_batch, batch_size=len(test_batch))
            for i in range(len(test_batch)):
                y = test_batch[i]
                tom = y_predicted[i][0].item()
                jerry = y_predicted[i][1].item()
                if y[0] + precision > tom > y[0] - precision and y[1] + precision > jerry > y[1] - precision:
                    results += 1
        return results / len(test_batch)


