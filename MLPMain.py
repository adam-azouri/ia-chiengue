import utils
import CNN
import torch
import time
import matplotlib.pyplot as plt


#train_results = utils.load_training_results("archive/ground_truth.csv")
#test_results = utils.load_testing_results("archive/ground_truth.csv")
#batched_train_results = utils.batch_data(train_results, 100)
#
#start = time.time()
#images = utils.load_batch_images(batched_train_results[0])
#in_size = images[0].shape[0]
#images = torch.from_numpy(images)
#results =batched_train_results[0].iloc[:, -1].values.tolist()
#mlp = MLP.MLP(in_size, [100, 50])
#loss = mlp.train_on_batch(images, torch.tensor(results, dtype=torch.float32))
#print(loss)
#test_images = utils.load_batch_images(test_results)
#test_images = torch.from_numpy(test_images)
#test_results = test_results.iloc[:, -1].values.tolist()
#print(mlp.test_images(test_images, torch.tensor(test_results, dtype=torch.float32)))
#end = time.time()
#print(end - start)

train_results = utils.load_training_results("archive/ground_truth.csv")
test_results = utils.load_testing_results("archive/ground_truth.csv")
batched_train_results = utils.batch_data(train_results, 100)
test_images = utils.load_batch_images_3D(test_results, folder="archive/data")
test_images = torch.from_numpy(test_images)
test_images = test_images.permute(0,3,1,2)
test_results = test_results.iloc[:, -1].values.tolist()
test_results = torch.tensor(test_results, dtype=torch.float32)


images = utils.load_batch_images_3D(batched_train_results[0], folder="archive/data")
in_size = images[0].shape[0]
images = torch.from_numpy(images)
resolution = (images.shape[1], images.shape[2])
cnn = CNN.CNN(resolution, lr=0.001)

start = time.time()
accuracy = []
loss = []
for j in range(100):
    for i in range(len(batched_train_results)):
        if i != 0 or j != 0:
            images = utils.load_batch_images(batched_train_results[i], folder="archive/data")
            images = torch.from_numpy(images)
            print(images.shape)
        images = images.permute(0,3,1,2)
        results =torch.tensor(batched_train_results[i].iloc[:,-1].values.tolist(), dtype=torch.float32)
        loss.append(cnn.train_on_batch(images, results, batch_size=100))
    acc = 0
    for k in range(test_results.shape[0]):
        acc += cnn.test(test_images[k], test_results[k])*100
    accuracy.append(acc)
    print("accuracy: ", acc, " loss: ",loss[-1])

end = time.time()
print(end - start)

plt.plot(accuracy)
plt.show()
plt.plot(loss)
plt.show()

torch.save(cnn, 'mlp_trained.pt')
