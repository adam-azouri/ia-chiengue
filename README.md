# Tom ou Jerry?

![img.png](img.png)

Voici le code source du projet transfer learning. Voici à quoi correspondent les fichiers:

## try-me.py
Programme permettant de tester les modèles. Il suffit de glisser déposer une image dans la zone prévue à cet effet.

## ftnn/*
### ftnn/fine-tuned-resnet-10-iterations.pt
Le modèle basé sur ResNet-18, entraîné sur 10 itérations.

### ftnn/fine-tuned-resnet-25-iterations.pt
Le modèle basé sur ResNet-50, entraîné sur 25 itérations.

## mccnn/*
### mccnn/mccnn_trained_50.pt
Réseau convolutionnel multilabel entraîné sur 50 itérations.

### mccnn/mccnn_trained_50_enhanced.pt
Réseau convolutionnel multilabel entraîné sur 50 itérations, avec plus d'images.

## test_images/*
Dossier d'images permettant de tester les modèles.

## CNN.py
Code source du réseau convolutionnel binaire.

## FTNN.py
Code source du réseau basé sur ResNet-50. (Fine tuned neural network)

## FTNNResNet18.py
Code source du réseau basé sur ResNet-18. (Fine tuned neural network)

## MCCNN.py
Code source du réseau convolutionnel multilabel.

## MLP.py
Code source du perceptron multicouche simple.

## MLPMain.py
Code d'ébauche pour les exécutions du programme sur kaggle

## TLNN.py
Code source d'un réseau basé sur Resnet-18, avec un perceptron recevant en entrée les sorties de Resnet-18.

