import torch
from torchvision.models import ResNet50_Weights


class TLNN():
    #a neural network using transfer learning from pretrained resnet50 (1000 classes)
    def __init__(self, resolution, lr=0.005, dropout = 0.5):
        self.resolution = resolution
        self.loss = torch.nn.CrossEntropyLoss()
        self.prediction_model = torch.hub.load('pytorch/vision:v0.6.0', 'resnet50', weights=ResNet50_Weights.DEFAULT)
        self.learning_model = torch.nn.Sequential()
        self.learning_model.add_module("fc1", torch.nn.Linear(1000, 500))
        self.learning_model.add_module("relu1" , torch.nn.ReLU())
        self.learning_model.add_module("fc2", torch.nn.Linear(500, 250))
        self.learning_model.add_module("relu2" , torch.nn.ReLU())
        self.learning_model.add_module("dropout", torch.nn.Dropout(dropout))
        self.learning_model.add_module("fc3", torch.nn.Linear(250, 2))
        self.optimizer = torch.optim.Adam(self.learning_model.parameters(), lr=lr)
        self.prediction_model.eval()

    def forward(self, x):
        self.learning_model.train()
        result = self.prediction_model(x)
        result = self.learning_model(result)
        return result

    def predict(self, x, batch_size=1):
        self.learning_model.eval()
        with torch.no_grad():
            return self.forward(x)

    def train_on_batch(self, image_batch, result_batch):
        self.learning_model.train()
        self.optimizer.zero_grad()
        y_pred = self.forward(image_batch)
        loss = self.loss(y_pred, result_batch)
        loss.backward()
        self.optimizer.step()
        return loss.item()

    def test(self, image_batch, test_batch, precision = 0.5):
        results = 0
        with torch.no_grad():
            y_predicted = self.predict(image_batch, batch_size=len(test_batch))
            for i in range(len(test_batch)):
                y = test_batch[i]
                tom = y_predicted[i][0].item()
                jerry = y_predicted[i][1].item()
                if y[0] + precision > tom > y[0] - precision and y[1] + precision > jerry > y[1] - precision:
                    results += 1
        return results / len(test_batch)