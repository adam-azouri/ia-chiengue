import torch
from CNN import CNN

class MCCNN(CNN):
    def __init__(self, resolution, lr=0.005, dropout = 0.5):
        super().__init__(resolution, lr, dropout)

    def predict(self, x, batch_size=1):
        self.eval()
        with torch.no_grad():
            return self.forward(x, batch_size=batch_size)

    def test(self, image_batch, test_batch, precision = 0.5):
        results = 0
        with torch.no_grad():
            y_predicted = self.predict(image_batch, batch_size=len(test_batch))
            for i in range(len(test_batch)):
                y = test_batch[i]
                tom = y_predicted[i][0].item()
                jerry = y_predicted[i][1].item()
                if y[0] + precision > tom > y[0] - precision and y[1] + precision > jerry > y[1] - precision:
                    results += 1
        return results / len(test_batch)