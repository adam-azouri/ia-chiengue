import tkinter as tk
from tkinterdnd2 import TkinterDnD, DND_FILES
import MCCNN
import FTNN
from PIL import Image, ImageTk
import torch
from  FTNNResNet18 import FTNNResNet18
import numpy as np


dropped_file_path = None
options = ["CNN entrainé sur le set de base", "CNN entrainé avec un set augmenté", "ResNet18", "ResNet50"]
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
models = [torch.load("mccnn/mccnn_trained_50.pt", map_location=device),
          torch.load("mccnn/mccnn_trained_50_enhanced.pt", map_location=device),
          torch.load("ftnn/fine-tuned-resnet-10-iterations.pt", map_location=device),
          torch.load("ftnn/fine-tuned-resnet-25-iterations.pt", map_location=device),]
models[3].__class__ = FTNNResNet18
def on_drop(event):
    global dropped_file_path
    file_path = event.data
    dropped_file_path = file_path.strip('{}')
    display_image(dropped_file_path)
    display_results(nn_prediction())

def display_image(file_path):
    # Charger l'image depuis le fichier
    image = Image.open(file_path)
    image.thumbnail((300, 300))  # Ajuster la taille de l'image selon vos besoins

    # Convertir l'image en format Tkinter
    tk_image = ImageTk.PhotoImage(image)

    # Mettre à jour l'étiquette avec la nouvelle image
    image_label.configure(image=tk_image)
    image_label.image = tk_image

def display_results(results):
    global table
    for j in range(4):
        for i in range(2):
            table[i+1][j+1].configure(text=f"{int(results[j][i]*100)}%")

def nn_prediction():
    global models
    global dropped_file_path
    image = Image.open(dropped_file_path)
    image = image.resize((352,240))
    image = image.convert('RGB')
    image = torch.from_numpy(np.array(image)).permute(2, 0, 1)
    image = image.unsqueeze(0)
    image = image.float()
    image = image.to(device)
    outputs =[]
    for model in models:
        outputs.append(model.predict(image).detach().cpu().numpy()[0])
    return outputs

root = TkinterDnD.Tk()
root.title("Tom ou Jerry ?")

# Créer une étiquette pour afficher l'image
image_label = tk.Label(root, borderwidth=2, relief="solid")
image_label.pack(padx=10, pady=10, expand=True)
image_label.configure(text="Glissez-déposez une image ici")
image_label.configure(font=("Arial", 18))

# Configurer l'étiquette pour accepter les fichiers d'image déposés
image_label.drop_target_register(DND_FILES)
image_label.dnd_bind("<<Drop>>", on_drop)

# Ajouter une zone de texte
grid_container = tk.Frame(root)
grid_container.pack()
content = [["Confiance \ Modèle", "CNN (set de base)", "CNN (set augmenté)", "ResNet18", "ResNet50"],
            ["Tom", "0%", "0%", "0%", "0%"],
            ["Jerry", "0%", "0%", "0%", "0%"]]
table = []
for i in range(3):
    row = []
    for j in range(5):
        label = tk.Label(grid_container, text=content[i][j], borderwidth=1, relief="solid", width=15, height=2)
        label.grid(row=i, column=j, padx=1, pady=1)
        row.append(label)
    table.append(row)


root.minsize(400, 400)

root.mainloop()
