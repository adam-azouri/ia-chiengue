import torch


class CNN(torch.nn.Module):
    """
        A CNN for classifying colored images. (2 classes, multiclass classification)
    """

    def __init__(self, resolution, lr=0.005, dropout = 0.5):
        """
            Initialize the CNN.
        """
        res = (resolution[0], resolution[1])
        super(CNN, self).__init__()
        self.loss = torch.nn.CrossEntropyLoss()
        start = 16

        #première couche avec 16 filtres, noyau 3x3 (activation relu)
        self.conv1 = torch.nn.Conv2d(3, start, 3)
        self.dropout = torch.nn.Dropout(dropout)
        res = (res[0] - 2, res[1] - 2)
        res = (res[0] // 2, res[1] // 2)
        self.relu = torch.nn.ReLU()
        #deuxième couche avec 32 filtres, noyau 3x3 (activation relu)
        self.conv2 = torch.nn.Conv2d(start, start*2, 3)
        res = (res[0] - 2, res[1] - 2)
        self.batchnorm2 = torch.nn.BatchNorm2d(start*2)
        self.maxpool = torch.nn.MaxPool2d(2)
        res= (res[0] // 2, res[1] // 2)
        #troisième couche avec 64 filtres, noyau 3x3 (elle passera dans le max pool aussi) (activation relu)
        self.conv3 = torch.nn.Conv2d(start*2, start*4, 3)
        self.batchnorm3 = torch.nn.BatchNorm2d(start*4)
        res = (res[0] - 2, res[1] - 2)
        res = (res[0] // 2, res[1] // 2)
        #couche dense (activation relu)
        self.fc1 = torch.nn.Linear(start*4*res[1]*res[0], start*4)
        #couche de sortie
        self.fc2 = torch.nn.Linear(start*4, 2)
        self.optimizer = torch.optim.Adam(self.parameters(), lr=lr)
        self.sigmoid = torch.nn.Sigmoid()


    def forward(self, x, batch_size=100):
        """
            Forward pass of the CNN.
        """
        x = self.conv1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.conv2(x)
        x = self.batchnorm2(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.conv3(x)
        x = self.batchnorm3(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = x.reshape(batch_size, -1)
        x = self.fc1(x)
        x = self.relu(x)
        x = self.dropout(x)
        x = self.fc2(x)
        x = self.sigmoid(x)
        return x

    def predict(self, x, batch_size=1):
        """
            Predict the output of the CNN.
        """
        self.eval()
        with torch.no_grad():
            return torch.argmax(self.forward(x, batch_size=batch_size), dim=1)

    def train_on_batch(self, image_batch, result_batch):
        """
            Train the CNN on a batch of data.
        """
        self.train()
        self.optimizer.zero_grad()
        y_pred = self.forward(image_batch)
        loss = self.loss(y_pred, result_batch)
        loss.backward()
        self.optimizer.step()
        return loss.item()

    def test(self, image_batch, test_batch):
        """
            Test the CNN on a batch of data.
        """
        results = 0
        with torch.no_grad():
            y_predicted = self.predict(image_batch, batch_size=len(test_batch))
            for i in range(len(test_batch)):
                y = test_batch[i]
                y_pred = y_predicted[i].item()
                results += y[y_pred]
        return results / len(test_batch)





