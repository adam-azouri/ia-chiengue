import torch

class MLP(torch.nn.Module):
    def __init__(self, input_size, sizes, lr=0.005):
        """
        Initialize the MLP.
        """
        super(MLP, self).__init__()
        self.input_size = input_size
        self.loss = torch.nn.CrossEntropyLoss()

        self.layers = torch.nn.Sequential()
        self.layers.add_module("fc1", torch.nn.Linear(input_size, sizes[0]))
        self.layers.add_module("relu1" , torch.nn.ReLU())
        for i in range(len(sizes) - 1):
            self.layers.add_module("fc" + str(i + 2), torch.nn.Linear(sizes[i], sizes[i + 1]))
            self.layers.add_module("relu" + str(i +1), torch.nn.ReLU())
        self.layers.add_module("fc" + str(len(sizes) + 1), torch.nn.Linear(sizes[-1], 2))
        self.layers.add_module("softmax", torch.nn.Softmax(dim=1))

        self.optimizer = torch.optim.Adam(self.parameters(), lr=lr)
    def forward(self, x):
        """
        Forward pass of the MLP.
        """
        return self.layers(x)

    def predict(self, x):
        """
        Predict the output of the MLP.
        """
        self.eval()
        with torch.no_grad():
            return torch.argmax(self.forward(x), dim=1)

    def train_on_batch(self, image_batch, result_batch):
        """
        Train the MLP on a batch of data.
        """
        self.train()
        self.optimizer.zero_grad()
        y_pred = self.forward(image_batch)
        loss = self.loss(y_pred, result_batch)
        loss.backward()
        self.optimizer.step()
        return loss.item()

    def test(self, image_batch, test_batch):
        """
        Test the MLP on a batch of data.
        """
        results = 0
        with torch.no_grad():
            y_predicted = self.predict(image_batch)
            for i in range(len(test_batch)):
                y = test_batch[i]
                y_pred = y_predicted[i].item()
                results += y[y_pred]
        return results / len(test_batch)



